package com.spring;

import org.springframework.context.ApplicationContext; 
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new FileSystemXmlApplicationContext(
				"src/main/java/com/spring/cfgs/config.xml");

		DBopeartionBean dbOperation =  (DBopeartionBean) context.getBean("studentJdbcTemplate");

		Student st = new Student();
		st.setSid(4);
		st.setName("Tony Stark");
		st.setAdd("NY");
		
		System.out.println(dbOperation.insert(st));
	}
}
